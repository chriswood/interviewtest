﻿using System;
using System.Runtime.Serialization;

namespace Maze {
	[Serializable]
	internal class MapDefinitionException : Exception {
		public MapDefinitionException() {
		}

		public MapDefinitionException(string message) : base(message) {
		}

		public MapDefinitionException(string message, Exception innerException) : base(message, innerException) {
		}

		protected MapDefinitionException(SerializationInfo info, StreamingContext context) : base(info, context) {
		}
	}
}