﻿using System;
using System.Runtime.Serialization;

namespace Maze {
	[Serializable]
	internal class TaskDefinitionException : Exception {
		public TaskDefinitionException() {
		}

		public TaskDefinitionException(string message) : base(message) {
		}

		public TaskDefinitionException(string message, Exception innerException) : base(message, innerException) {
		}

		protected TaskDefinitionException(SerializationInfo info, StreamingContext context) : base(info, context) {
		}
	}
}