﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Maze {
	public class Map {
		// The rooms that make up this map
		private Dictionary<int, Room> _rooms = new Dictionary<int, Room>();

		/// <summary>
		/// Creates a new Map from the specified XML file
		/// </summary>
		/// <param name="path">The path to the file that contains the Map</param>
		public Map(string path) {
			ReadMap(path);
		}

		private void ReadMap(string path) {
			using (var stream = File.OpenRead(path)) {
				try {
					// Load the map into an XDocument
					var xmlDocument = XDocument.Load(stream);

					// Find the root Map element
					var mapElement = xmlDocument.Element("Map");

					if (mapElement == null) throw new MapDefinitionException("Document must contain a root Map element");

					// Process all of the Room elements within the Map
					foreach (var roomElement in mapElement.Elements("Room")) {
						var room = new Room() {
							Id = Convert.ToInt32(roomElement.Attribute("Id").Value),
							Name = roomElement.Attribute("Name").Value
						};

						// The Attribute names of the Room element correspond to the names in the Direction enum.
						// Loop through all of the possible direction names.
						foreach (Direction direction in Enum.GetValues(typeof(Direction))) {
							var directionAttribute = roomElement.Attribute(Enum.GetName(typeof(Direction), direction));

							// If there is an attribute with this direction name then we can leave this room in that direction
							if (directionAttribute != null) {
								room.Exits[direction] = Convert.ToInt32(directionAttribute.Value);
							}
						}

						// Loop through any Object elements within the room and create an Item to represent them
						foreach (var objectElement in roomElement.Elements("Object")) {
							room.Items.Add(
								new Item() {
									Name = objectElement.Attribute("Name").Value
								}
							);
						}

						// Add the newly imported room to the dictionary keyed with it's Id
						_rooms.Add(room.Id, room);
					}
				} catch (Exception ex) {
					// Wrap the exception in a MapDefinitionException so that we know it was caused by invalid
					// file contents.
					throw new MapDefinitionException("Exception importing map definition", ex);
				}
			}
		}

		/// <summary>
		/// Attempt to explore the map from the specified starting location and collection all of the listed items.
		/// </summary>
		/// <param name="task">The defintion of the task to perform.</param>
		/// <returns>An IEnumerable of the acts that are required to complete the task if all items were found, null on if any items weren't found.</returns>
		public IEnumerable<Act> PerformTask(TaskDefinition task) {
			Room startRoom = _rooms[task.StartRoom];

			var acts = new List<Act>();

			// Record the starting of the exploration as an Act
			acts.Add(new Start() { Room = startRoom });

			// Create a copy of the list of items that we must find
			var remainingItems = (from x in task.ItemNames select x).ToList();

			// Perform the actual exploration
			var success = Explore(startRoom, remainingItems, acts, new List<int>());

			if (success) {
				// All of the specified items were found
				return acts;
			} else {
				// One or more of the specified items wasn't found
				return null;
			}
		}

		/// <summary>
		/// Performs a recursive depth first search of the map
		/// </summary>
		/// <param name="room">The room that we are currently exploring</param>
		/// <param name="remainingItems">The items that we have not yet found</param>
		/// <param name="acts">The acts that we have taken so far during our exploration</param>
		/// <param name="visitedRoomNumbers">The Ids of the rooms that we have already visited</param>
		/// <returns>true if all of the items have been found, false otherwise</returns>
		private bool Explore(Room room, List<string> remainingItems, List<Act> acts, List<int> visitedRoomNumbers) {
			// Print the fact that we have discovered this room during our exploration
			Console.WriteLine($"Discovered the {room.Name}");

			// Record the fact that we have visited this room so that we don't end up with a stack overflow
			visitedRoomNumbers.Add(room.Id);

			var collectedItems = new List<Item>();

			// Collect any items that are in this room
			foreach (var item in room.Items) {
				if (remainingItems.Contains(item.Name)) {
					// We have found an item that we were looking for, remove it from
					// the list of items that we have yet to find.
					remainingItems.Remove(item.Name);

					// Add the item to the list of those collected from this room
					collectedItems.Add(item);
				}
			}

			if (collectedItems.Any()) {
				// If we collected any items then record this fact
				acts.Add(new Collect() {
					Items = collectedItems
				});

				// If we have no more items left to find then we have completed the task
				if (!remainingItems.Any()) {
					return true;
				}
			}

			// Loop through all of the directions through which it might be possible to exit this room
			foreach (Direction direction in Enum.GetValues(typeof(Direction))) {
				if (room.Exits.ContainsKey(direction)) {
					// We can exit in this direction to another room
					var nextRoom = _rooms[room.Exits[direction]];

					// If we haven't already visited this room then we should explore it
					if (!visitedRoomNumbers.Contains(nextRoom.Id)) {
						// Record the number of acts that we have performed so far and the number of items
						// we have left to collect. This enables us to prune any branches from this node
						// that don't result in us finding an item i.e. rooms that don't have to be visited
						// in order to collect all of the items.
						int numberOfRemainingItems = remainingItems.Count;
						int numberOfActs = acts.Count;

						// Record that fact that we are moving into a new room
						acts.Add(new Move() {
							Direction = direction,
							Room = nextRoom
						});

						// Recursively explore the new room
						if (Explore(nextRoom, remainingItems, acts, visitedRoomNumbers)) {
							// If Explore has returned true then we now have all of the items and can simply return true ourselves.
							return true;
						} else {
							// If we still haven't found all of the items then record the fact that we have moved back into this.
							// room in order to continue our search.
							acts.Add(new Move() {
								Direction = DirectionUtils.Opposite(direction),
								Room = room
							});
						}

						// Test to see if we found any items along that branch.
						if (numberOfRemainingItems == remainingItems.Count) {
							// We did't find any items along that branch therefor we can remove those moves.
							acts.RemoveRange(numberOfActs, acts.Count - numberOfActs);
						}
					}
				}
			}

			// We haven't yet found all of the items but we have done everything we can along this branch
			return false;
		}
	}
}
