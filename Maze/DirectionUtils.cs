﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maze {
	public static class DirectionUtils {
		public static Direction Opposite(Direction direction) {
			switch(direction) {
				case Direction.East: return Direction.West;
				case Direction.West: return Direction.East;
				case Direction.North: return Direction.South;
				case Direction.South: return Direction.North;
				default: throw new NotImplementedException("Unhandled direction");
			}
		}
	}
}
