﻿using System;
using System.Runtime.Serialization;

namespace Maze {
	[Serializable]
	internal class UnhandledActException : Exception {
		public UnhandledActException() {
		}

		public UnhandledActException(string message) : base(message) {
		}

		public UnhandledActException(string message, Exception innerException) : base(message, innerException) {
		}

		protected UnhandledActException(SerializationInfo info, StreamingContext context) : base(info, context) {
		}
	}
}