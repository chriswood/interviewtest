﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maze {
	public class TaskDefinition {
		/// <summary>
		/// Create a new TaskDefinition from the specified text file.
		/// </summary>
		/// <param name="path">The path to the file containing the definition of the task.</param>
		public TaskDefinition(string path) {
			ReadTask(path);
		}

		private void ReadTask(string path) {
			using(var stream = File.OpenRead(path)) {
				using(var reader = new StreamReader(stream)) {
					try {
						string startRoomLine = reader.ReadLine().Trim();

						StartRoom = Convert.ToInt32(startRoomLine);

						while (!reader.EndOfStream) {
							string itemName = reader.ReadLine().Trim();

							if (!string.IsNullOrEmpty(itemName)) {
								ItemNames.Add(itemName);
							}
						}
					} catch (Exception ex) {
						throw new TaskDefinitionException("Invalid task definition", ex);
					}
				}
			}
		}

		public int StartRoom { get; set; }

		public List<string> ItemNames { get; private set; } = new List<string>();
	}
}
