﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maze {
	public class Room {
		public int Id { get; set; }

		public string Name { get; set; }

		public Dictionary<Direction, int> Exits { get; private set; } = new Dictionary<Direction, int>();

		public List<Item> Items { get; private set; } = new List<Item>();

		public bool HasMultipleExits {
			get {
				return Exits.Count > 1;
			}
		}
	}
}
