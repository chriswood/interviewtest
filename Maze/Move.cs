﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maze {
	public class Move : Act {
		public Direction Direction { get; set; }

		public Room Room { get; set; }
	}
}
