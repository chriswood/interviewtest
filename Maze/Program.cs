﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maze {
	class Program {
		static void Main(string[] args) {
			// Load the original map as specified for the interview
			var originalMap = new Map("Data/Map.Original.xml");

			// The the extended map that has been created to demonstrate the 'pruning' of
			// unsuccessfull branches of exploration. This is shown by the discovering of
			// the Porch and Garden by leaving the hallway to the south but not including
			// these in the output of the solution.
			var expandedMap = new Map("Data/Map.Expanded.xml");

			// Load the definition of the task that we must perform
			var taskDefinition = new TaskDefinition("Data/Task.txt");

			// Perform the task on the original interview map
			Console.WriteLine("******* Exploring Original Map *********");

			var actsForOriginal = originalMap.PerformTask(taskDefinition);

			// Print the result of the exploration
			Console.WriteLine();
			PrintResult(actsForOriginal);

			// Perform the same task on the extended map
			Console.WriteLine();
			Console.WriteLine("******* Exploring Extended Map *********");

			var actsForExtended = expandedMap.PerformTask(taskDefinition);

			// Print the result of the exploration
			Console.WriteLine();
			PrintResult(actsForExtended);

			Console.WriteLine();
			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}

		static void PrintResult(IEnumerable<Act> acts) {
			if (acts != null) {
				Console.WriteLine("Found all of the items by taking the following route:");

				// Print actions that we took during the exploration
				PrintActs(acts);
			} else {
				Console.WriteLine("Failed to find all items.");
			}
		}

		static void PrintActs(IEnumerable<Act> acts) {
			foreach (var act in acts) {
				if (act is Start) {
					var start = (Start)act;

					Console.WriteLine("Start in the " + start.Room.Name);
				} else if (act is Move) {
					var move = (Move)act;

					Console.WriteLine($"Move {Enum.GetName(typeof(Direction), move.Direction)} to the {move.Room.Name}");
				} else if (act is Collect) {
					var collect = (Collect)act;

					foreach (var item in collect.Items) {
						Console.WriteLine($"Collect the {item.Name}");
					}
				} else throw new UnhandledActException(typeof(Act).Name);
			}
		}
	}
}
